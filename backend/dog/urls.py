from django.urls import path
from .views import GetRandomDog

urlpatterns = [
    path("", GetRandomDog.as_view()),
]
               