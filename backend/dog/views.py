from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from .models import Dog
from walk.models import Walk
from walk.serializers import WalkSerializer
from .serializers import DogSerializer
import random


class GetRandomDog(APIView):
    def get(self, request):
        dogs = Dog.objects.all()
        random_number = random.randint(0, dogs.count() - 1)

        dog = dogs[random_number]
        available_walks = Walk.objects.filter(dog=dog, is_reserved=False)
        response_data = DogSerializer(dog).data
        response_data["walks"] = None
        
        if available_walks:
            walks = WalkSerializer(available_walks, many=True).data
            response_data["walks"] = walks

        return Response(response_data, status=status.HTTP_200_OK)
