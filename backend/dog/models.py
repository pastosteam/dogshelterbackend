from django.db import models


class Dog(models.Model):
    SEX = [("MALE", "MALE"), ("FEMALE", "FEMALE")]

    name = models.CharField(max_length=20)
    age = models.PositiveIntegerField()
    description = models.TextField(max_length=2000)
    sex = models.CharField(choices=SEX, max_length=15)
