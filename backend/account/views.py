from django.shortcuts import render
from rest_framework import status
from rest_framework.views import APIView
from .serializers import LoginSerializer
from rest_framework.response import Response

# [POST] Login API View
# End point needs password and user's email


class LoginView(APIView):
    def post(self, request, format=None):
        serializer = LoginSerializer(
            data=self.request.data, context={"request": self.request}
        )
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]

        return Response(
            {
                "id": user.id,
                "email": user.email,
                "nickname": user.nickname,
            },
            status=status.HTTP_200_OK,
        )
