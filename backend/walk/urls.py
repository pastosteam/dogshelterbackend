from django.urls import path
from .views import ReserveWalk

urlpatterns = [
    path("<int:pk>/", ReserveWalk.as_view()),
]
