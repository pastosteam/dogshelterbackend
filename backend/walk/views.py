from django.shortcuts import render
from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Walk
from account.models import Account
from rest_framework import status
from dog.models import Dog
from django.core.exceptions import ObjectDoesNotExist
from django.utils.datastructures import MultiValueDictKeyError


class ReserveWalk(APIView):
    def put(self, request, pk):
        try:
            walk = Walk.objects.get(pk=pk)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_id = request.data["user"]
        except MultiValueDictKeyError:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        try:
            user = Account.objects.get(pk=user_id)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        walk.user = user
        walk.is_reserved = True
        walk.save()
        return Response(status=status.HTTP_200_OK)
