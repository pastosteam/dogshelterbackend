from django.db import models
from dog.models import Dog
from django.conf import settings


class Walk(models.Model):
    date = models.DateTimeField()
    dog = models.ForeignKey(Dog, on_delete=models.CASCADE, related_name="dog_id")
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        default=None,
        on_delete=models.SET_NULL,
        related_name="user_id",
    )
    is_reserved = models.BooleanField(default=False)
